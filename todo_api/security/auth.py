from todo_api.models import User, Todo

"""
Generate a endpoint `/auth` which accepts 2 params:
username and password of the user
if the user is found in the database, return the found user
"""
def authenticate(username, password):
     if username:
        user = User.query.filter_by(username=username).first()
        
        from todo_api import bcrypt

        if bcrypt.check_password_hash(user.password, password):
            return user

"""
Data stored inside a JWT is called a "payload", 
so our identity function accepts that payload as a parameter.
Called when a request is made to validate the JWT
"""
def identity(payload):
    user_id = payload["identity"]
    
    return User.query.filter_by(id=user_id).first()