import os
import psycopg2
from flask_api import FlaskAPI
from flask_cors import CORS
from flask_sqlalchemy import SQLAlchemy
from flask_bcrypt import Bcrypt
from flask_jwt import JWT
from datetime import timedelta

app = FlaskAPI(__name__)
app.secret_key = "2G2luAh8EzNv3aCwOk7YrRKKKJdslVta"

app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
app.config['JWT_SECRET_KEY'] = 'hiber4nate-wh7ite-zargaba3ath'
app.config['JWT_EXPIRATION_DELTA'] = timedelta(seconds=86400) # 24h
# Create local file for database
app.config['SQLALCHEMY_DATABASE_URI'] = os.environ['DATABASE_URL']

CORS(app, origins='*', 
     headers=['Content-Type', 'Authorization'], 
     expose_headers='Authorization')

# Instance of our local database
db = SQLAlchemy(app)

from todo_api.security.auth import authenticate, identity

jwt = JWT(app, authenticate, identity)
bcrypt = Bcrypt(app)

# Make the import of routes at the end to avoid circular imports
from todo_api import routes
