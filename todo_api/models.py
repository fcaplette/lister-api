from datetime import datetime

from todo_api import db


class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(120), unique=True, nullable=False)
    password = db.Column(db.String(60), nullable=False)

    todos = db.relationship("Todo", backref="author", lazy=True)

    def __repr__(self):
        return f"User('id: {self.id}' 'username: {self.username}', 'password: {self.password}')"


class Todo(db.Model):
    todo_id = db.Column(db.Integer, primary_key=True)
    text = db.Column(
        db.String(100), nullable=False)
    date = db.Column(db.String, nullable=True)
    priority = db.Column(db.String(20), nullable=False)
    completed = db.Column(db.Boolean, nullable=False)

    # The id of the author who create the todo
    user_id = db.Column(db.Integer, db.ForeignKey("user.id"), nullable=False)

    def __repr__(self):
        return f"Todo('text:{self.text}', 'date: {self.date}', 'priority: {self.priority}' 'id: {self.user_id}' 'completed: {self.completed}')"
