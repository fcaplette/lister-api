import json
from flask import Response, request, make_response
from flask_jwt import jwt_required, current_identity

from todo_api import app, db, bcrypt
from todo_api.models import User, Todo


@app.route("/signup/", methods=["POST"])
def signup():
    payload_data = request.get_json()
    username = payload_data["username"]
    user = User.query.filter_by(username=username).first()

    if user:
        return make_response({"description": "An account with this username already exists."}, 403)
    else:
        password = payload_data["password"]
        hashed_password = bcrypt.generate_password_hash(password).decode("UTF-8")
        user = User(username=username, password=hashed_password)

        db.session.add(user)
        db.session.commit()

        return make_response({}, 201)

@app.route("/add_todo/", methods=["POST"])
@jwt_required()
def add_todo():
    try:
        payload_data = request.get_json()

        new_todo = payload_data["todo"]
        id = payload_data["user_id"]

        todo = Todo(
            text=new_todo["text"], 
            date=new_todo["date"], 
            priority=new_todo["priority"], 
            completed=False,
            user_id=id
            )

        db.session.add(todo)
        db.session.commit()

        return make_response({"id": todo.todo_id}, 201)

    except:
        return make_response({"description": "Could not add your todo"}, 400)

@app.route("/delete_todo/", methods=["DELETE"])
@jwt_required()
def delete_todo():
    try:
        payload_data = request.get_json()
        todo_id = payload_data["id"]

        todo = Todo.query.filter_by(todo_id=todo_id).first()
        
        db.session.delete(todo)
        db.session.commit()

        return make_response({}, 200)

    except:
        return make_response({"description": "Could not find the todo to delete"}, 404)

@app.route("/todo/", methods=["PATCH"])
@jwt_required()
def todo():
    try:
        payload_data = request.get_json()

        todo_to_update = payload_data["todo"]
        user_id = payload_data["user_id"]
        user = User.query.filter_by(id=user_id).first()
        todo = Todo.query.filter_by(todo_id=todo_to_update["id"]).first()

        if todo_to_update["text"]:
            todo.text = todo_to_update["text"]

        if todo_to_update["date"]:
            todo.date = todo_to_update["date"]

        if todo_to_update["priority"] or todo_to_update["priority"] is 0:
            todo.priority = todo_to_update["priority"]

        if todo_to_update["completed"] is not None:
            todo.completed = todo_to_update["completed"]

        db.session.commit()

        return make_response({}, 201)

    except:
        return make_response({"description": "Could not patch your todo"}, 400)


@app.route("/user/", methods=["GET"])
@jwt_required()
def user():    
    try:
        if current_identity:
            todos = Todo.query.filter_by(user_id=current_identity.id).all()
            todos_struct = [
                {
                    "id": todo.todo_id, 
                    "text": todo.text, 
                    "date": todo.date, 
                    "priority": int(todo.priority), 
                    "completed": todo.completed,
                } for todo in todos
                ]

        return make_response({"id": current_identity.id, "todos": todos_struct}, 200)

    except:
        return make_response({"description": "Could not fetch user"}, 400)